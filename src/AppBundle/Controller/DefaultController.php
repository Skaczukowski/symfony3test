<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Example;
use Psr\Log\LoggerInterface;
use AppBundle\Form\FormType;

class DefaultController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    public function indexAction()
    {
        return $this->render('AppBundle:Default:index.html.twig');
    }
    
    public function jsonAction()
    {
        try {
            $exampleRepo = $this->em->getRepository('AppBundle:Example')->findAll();
        } catch (\Exception $e) {
            $this->logger->error(sprintf('ERROR : Message: %s', $e->getMessage()));
        }

        return new JsonResponse($exampleRepo);
    }

    public function displayAction(Request $request)
    {
        $form = $this->createForm(FormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            
            $Example = new Example();
            $Example
                ->setName($formData['name']);

            $this->em->persist($Example);
            $this->em->flush();
        }

        try {
            $exampleRepo = $this->em->getRepository('AppBundle:Example')->findAll();
        } catch (\Exception $e) {
            $this->logger->error(sprintf('ERROR : Message: %s', $e->getMessage()));
        }

        return $this->render('AppBundle:Default:display.html.twig', [
            'example' => $exampleRepo,
            'form' => $form->createView(),
        ]);
    }
}

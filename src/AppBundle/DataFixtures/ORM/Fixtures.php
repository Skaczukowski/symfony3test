<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Example;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $product = new Example();
            $product->setName('example '.mt_rand(10, 100));
            $manager->persist($product);
        }

        $manager->flush();
    }
}